#! /usr/bin/env bash

# DB setup

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "recipientName": "Bob", "senderName": "Jay", "message": "Hello!" }' 'http://localhost:3000/api/messages'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{   "recipientName": "Jay",  "senderName": "Bob", "message": "Hay!" }' 'http://localhost:3000/api/messages'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "recipientName": "Bob", "senderName": "Jay", "message": "nice house there." }' 'http://localhost:3000/api/messages'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{   "recipientName": "Jay",  "senderName": "Bob", "message": "You could book an appointment on the link below..." }' 'http://localhost:3000/api/messages'

curl -X POST --header 'Content-Type: application/json' --header 'Accept: application/json' -d '{ "recipientName": "Bob", "senderName": "Jay", "message": "Sounds great!" }' 'http://localhost:3000/api/messages'
