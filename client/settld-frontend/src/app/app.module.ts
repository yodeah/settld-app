import { AppComponent } from './app.component';
import { Routes } from '@angular/router';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {
  Http,
  Response,
  RequestOptions,
  Headers,
  HttpModule
} from "@angular/http";
import { HttpClientModule } from "@angular/common/http";  
import { RouterModule } from "@angular/router";
import { FormsModule } from '@angular/forms';

import { MessageBoardComponent } from './components/message-board/message-board.component';
import { IndexComponent } from './components/index/index.component';
import { MessageService } from './service/message-service';
import { MessageBoardGuard } from './guards/message-board-guard';
import { NavbarComponent } from './components/navbar/navbar.component';
import { MessageComponent } from './components/message-board/message/message.component';


const routes: Routes = [
  { path: "", component: IndexComponent },
  {
    path: "messages",
    component: MessageBoardComponent,
    canActivate: [MessageBoardGuard]
  }
]

@NgModule({
  declarations: [
    IndexComponent,
    MessageBoardComponent,
    AppComponent,
    NavbarComponent,
    MessageComponent
  ],
  imports: [
    RouterModule.forRoot(routes, { useHash: true }),
    BrowserModule,
    HttpClientModule,
    HttpModule,
    FormsModule
  ],
  providers: [
    MessageBoardGuard,
    MessageService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
