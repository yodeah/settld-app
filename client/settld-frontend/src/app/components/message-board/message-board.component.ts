import { MessageModel } from './../../model/message-model';
import { MessageService } from './../../service/message-service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-message-board',
  templateUrl: './message-board.component.html',
  styleUrls: ['./message-board.component.css']
})
export class MessageBoardComponent implements OnInit {

  messages: Array<MessageModel>;

  constructor(private messageService: MessageService) { 
    this.messageService.getMessages()
    .then((messages: Array<MessageModel>) => { this.messages = messages; })
    .catch(() => alert("error while getting messages"));
  }

  ngOnInit() { }

}
