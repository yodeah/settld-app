import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { LocalStorageKeys } from '../../model/local-storage-keys';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  public username:string;

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public logIn(){
    localStorage.setItem(LocalStorageKeys.Username, this.username);
    this.router.navigate(['messages']);
  }

}
