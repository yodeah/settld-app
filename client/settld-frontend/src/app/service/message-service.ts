import { MessageModel } from './../model/message-model';
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";


@Injectable()
export class MessageService {

    readonly appUrl = "http://localhost:3000/api/"
    readonly messageEndpoint = "messages"

    constructor(private http: HttpClient) {}
    
    public getMessages():Promise<Array<MessageModel>>{
        let url = this.appUrl + this.messageEndpoint;
        return this.http.get<Array<MessageModel>>(url).toPromise();
    }

}