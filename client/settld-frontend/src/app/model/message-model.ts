export class MessageModel{
    public recipientName: string;
    public senderName: string;
    public message: string;
}