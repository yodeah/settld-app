import { LocalStorageKeys } from './../model/local-storage-keys';
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { Router } from '@angular/router';
import {
    CanActivate,
    ActivatedRouteSnapshot,
    RouterStateSnapshot
} from "@angular/router";

@Injectable()
export class MessageBoardGuard implements CanActivate {

    constructor(private router: Router) { }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): boolean | Observable<boolean> | Promise<boolean> {
        let username = localStorage.getItem(LocalStorageKeys.Username);

        console.log("guard usn", username);
        if (username === null || username.length === 0)  {
            console.log("no user logged in");
            this.router.navigate(['']);//index
            return false; 
        }
        console.log("user logged in");
        return true; 
    }
}
